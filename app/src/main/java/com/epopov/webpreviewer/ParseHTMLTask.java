package com.epopov.webpreviewer;


import android.os.AsyncTask;

import com.epopov.webpreviewer.Interfaces.IHandler;
import com.epopov.webpreviewer.Models.HtmlEntities;
import com.epopov.webpreviewer.Network.Utils;

import org.jsoup.nodes.Document;

public class ParseHTMLTask extends AsyncTask<String, Void, HtmlEntities> {
    private static final String TAG = ParseHTMLTask.class.getSimpleName();
    private HtmlEntities htmlEntities;
    private IHandler handler;

    public ParseHTMLTask(IHandler handler) {
        this.handler = handler;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        handler.showProgressIndicator();
    }

    @Override
    protected HtmlEntities doInBackground(String... params) {
        if (params.length == 0) return null;
        Document htmlDoc = Utils.getHtml(params[0]);
        htmlEntities = Utils.parseHtml(htmlDoc);
        htmlEntities.setImage(Utils.loadBitmap(htmlEntities.getImgUrl()));
        return htmlEntities;
    }

    @Override
    protected void onPostExecute(HtmlEntities htmlEntities) {
        super.onPostExecute(htmlEntities);
        handler.hideProgressIndicator();
        handler.updatePreview(htmlEntities);
    }
}
