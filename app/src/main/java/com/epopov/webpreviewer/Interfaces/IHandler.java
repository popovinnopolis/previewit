package com.epopov.webpreviewer.Interfaces;


import com.epopov.webpreviewer.Models.HtmlEntities;

public interface IHandler {
    void showProgressIndicator();
    void hideProgressIndicator();
    void updatePreview(HtmlEntities htmlEntities);
}
