package com.epopov.webpreviewer.Network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.epopov.webpreviewer.Models.HtmlEntities;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;


public class Utils {
    private static final String TAG = Utils.class.getSimpleName();

    public static String buildUrl(String inputUrl) {
        if (inputUrl.equals("")) return null;
        String scheme, authority;
        Uri builtUri = Uri.parse(inputUrl);
        scheme = builtUri.getScheme() == null ? "http" : builtUri.getScheme();
        authority = builtUri.getAuthority() == null ? inputUrl : builtUri.getAuthority();
        return new Uri.Builder().scheme(scheme).authority(authority).build().toString();
    }

    public static Document getHtml(String url) {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }

    public static HtmlEntities parseHtml(Document htmlDoc) {

        String title = htmlDoc.select("meta[property=og:title]").size() == 0 ?
                htmlDoc.title() :
                htmlDoc.select("meta[property=og:title]").first().attr("content");
        String description = htmlDoc.select("meta[name=description]").size() == 0 ?
                htmlDoc.select("h1, h2").first() == null ? "" : htmlDoc.select("h1, h2").first().text() :
                htmlDoc.select("meta[name=description]").first().attr("content");
        String imgUrl = htmlDoc.select("meta[property=og:image]").size() == 0 ?
                htmlDoc.select("img").first() == null ? "" : htmlDoc.select("img").first().attr("src") :
                htmlDoc.select("meta[property=og:image]").first().attr("content");

        return new HtmlEntities(title, description, imgUrl);
    }

    public static Bitmap loadBitmap(String imgUrl) {
        try {
            URL url = new URL(imgUrl);
            return BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }
}
