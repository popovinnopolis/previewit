package com.epopov.webpreviewer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.epopov.webpreviewer.Interfaces.IHandler;
import com.epopov.webpreviewer.Models.HtmlEntities;

import static com.epopov.webpreviewer.Network.Utils.buildUrl;

public class MainActivity extends AppCompatActivity implements IHandler{
    private EditText et_url;

    private TextView tvTitle;
    private ImageView ivImage;
    private TextView tvDescription;

    private ProgressBar pb_loading_indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_url = (EditText) findViewById(R.id.et_url);

        tvTitle = (TextView) findViewById(R.id.tvTitle);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        pb_loading_indicator = (ProgressBar) findViewById(R.id.pb_loading_indicator);
    }

    public void previewItClick(View view) {
        String url = buildUrl(et_url.getText().toString().trim());

        new ParseHTMLTask(this).execute(url);
    }

    @Override
    public void showProgressIndicator() {
        pb_loading_indicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressIndicator() {
        pb_loading_indicator.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updatePreview(HtmlEntities htmlEntities) {
        tvTitle.setText(htmlEntities.getTitle());
        tvDescription.setText(htmlEntities.getDescription());
        ivImage.setImageBitmap(htmlEntities.getImage());
    }

    public void webItClick(View view) {
        Intent intent = new Intent(this, WebViewActivity.class);
        intent.putExtra("link", et_url.getText().toString());
        startActivity(intent);
    }
}
